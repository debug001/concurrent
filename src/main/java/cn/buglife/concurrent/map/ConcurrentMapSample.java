package cn.buglife.concurrent.map;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * ConcurrentHashMap 和 java.util.HashTable 类很相似，但 ConcurrentHashMap 能够提供比
 * HashTable 更好的并发性能。
 * <p>
 * <p>
 * 在你从中读取对象的时候 ConcurrentHashMap 并不会把整个 Map 锁住。
 * </p>
 * <p>
 * 此外，在你向其中写入对象的时候，ConcurrentHashMap 也不会锁住整个 Map。它的内部只是把 Map 中正在被写入的部分进行锁定。
 * 另外一个不同点是，在被遍历的时候，即使是 ConcurrentHashMap 被改动，它也不会抛
 * ConcurrentModificationException。尽管 Iterator 的设计不是为多个线程的同时使用。
 * </p>
 *
 * @author zhangjun
 */
public class ConcurrentMapSample {

  public static void main(String[] args) {
    ConcurrentMap<String, String> map = new ConcurrentHashMap<>();
    Runnable producer = () -> {
      for (int i = 0; i < 5; i++) {
        map.put("k" + i, "v_" + i);
      }
    };

    for (int i = 0; i < 10; i++) {
      new Thread(producer).start();
    }

    Runnable consumer = () -> {
      Set<String> strs = map.keySet();
      for (String key : strs) {
        System.out.println(key);
        map.remove(String.valueOf(key));
      }
    };
    for (int i = 0; i < 10; i++) {
      new Thread(consumer).start();
    }
  }
}
