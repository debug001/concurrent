package cn.buglife.concurrent.map;

import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * ConcurrentNavigableMap是一个支持并发访问的 java.util.NavigableMap，它还能让它的子 map
 * 具备并发访问的能力。
 * 
 * 所谓的 "子 map" 指的是诸如 headMap()，subMap()，tailMap() 之类的方法返回的 map。
 * 
 * @author zhangjun
 */
public class ConcurrentNavigableMapSample {

  public static void main(String[] args) {
    ConcurrentNavigableMap<String, String> map = new ConcurrentSkipListMap<>();

    map.put("1", "one");
    map.put("2", "two");
    map.put("3", "three");

    // headMap(T toKey) 方法返回一个包含了小于给定 toKey 的 key 的子 map。
    ConcurrentNavigableMap<String, String> headMap = map.headMap("2");
    System.out.println(headMap.toString());

    // subMap() 方法返回原始 map 中，键介于 from(包含) 和 to (不包含) 之间的子 map。
    ConcurrentNavigableMap<String, String> subMap = map.subMap("2", "3");
    System.out.println(subMap.toString());

    // tailMap(T fromKey) 方法返回一个包含了不小于给定 fromKey 的 key 的子 map。
    ConcurrentNavigableMap<String, String> tailMap = map.tailMap("2");
    System.out.println(tailMap.toString());
  }
}
