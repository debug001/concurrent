package cn.buglife.concurrent.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * FixedThreadPool指定线程池大小，即线程数量是固定的，如果提交的任务数量超过了线程池的大小，则会将任务存入池队列（阻塞队列）中
 * 该类型的线程池完全达到了节省线程创建与销毁时间的效果。当然缺点就是，当线程空闲的适合不会自动结束进行销毁依然会占用系统资源
 *
 * Created by zhangjun on 2017/4/8.
 */
public class FixedThreadPoolSample {

  public static void main(String[] args) {
    ExecutorService executorService = Executors.newFixedThreadPool(10);

    //创建100个任务进入该线程池，我们会发现当0-99数字输出结束之后，只要我们不手动关闭控制台，则控制台会一直处于开启状态，不进行关闭，正是
    //因为该类线程池是固定大小的线程池，即使线程空闲的时候也不会自动释放线程资源
    //同时我们会根据打印出来的线程池大小看到线程池的大小是随着任务的增多递增直至线程数量达到指定的线程池大小10则不再进行增加
    for (int i = 0; i < 100; i++) {
      final int index = i;
      executorService.execute(()-> System.out.println("当前线程："+Thread.currentThread()+"------"+index));
      System.out.println("线程池大小:"+((ThreadPoolExecutor)executorService).getPoolSize());
    }
  }
}
