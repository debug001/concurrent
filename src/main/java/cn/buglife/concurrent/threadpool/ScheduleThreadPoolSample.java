package cn.buglife.concurrent.threadpool;

import java.util.concurrent.*;

/**
 * 定长线程池（newScheduleThreadPool）
 * 可定时以及周期性的执行任务
 *
 * Created by zhangjun on 2017/4/8.
 */
public class ScheduleThreadPoolSample {

  public static void main(String[] args) {
    //定义可容纳5个线程的定长线程池，即同时可执行五个定时任务
    ScheduledExecutorService executorService = Executors.newScheduledThreadPool(5);

    //以上定义了线程池大小为5的定长线程池，同时我们下面定义了100个任务延迟10秒执行并打印出时间差，理论时间差应该是10秒左右
    final long timestample = System.currentTimeMillis();

    for (int i = 0; i < 100; i++) {
       final int index = i;
      executorService.schedule(()->{
        System.out.println(Thread.currentThread()+"================任务"+index+"==========="+(System.currentTimeMillis()-timestample));
        System.out.println("线程池大小:"+((ThreadPoolExecutor)executorService).getPoolSize());
      },10, TimeUnit.SECONDS);
    }
  }
}
