/**
 * 线程池：
 * 可缓存线程池（newCachedThreadPool）
 * 指定工作线程数量的线程池（newFixedThreadPool）
 * 单线程化的Executor(newSingleThreadPool)
 * 定长线程池(newScheduleThreadPool)
 * <p>
 * Created by zhangjun on 2017/4/8.
 */
package cn.buglife.concurrent.threadpool;