package cn.buglife.concurrent.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 单线程化的Executor，只会创建一个线程进行执行任务
 * 可保证任务是按顺序执行
 *
 * Created by zhangjun on 2017/4/8.
 */
public class SingleThreadExecutorSample {


  public static void main(String[] args) {
    ExecutorService executorService = Executors.newSingleThreadExecutor();

    //放100个任务进入以上所创建的线程池，我们会发现在控制台依次打印出0-99的数字
    //且线程大小一直处于1个，同时也不会自动释放
    for (int i = 0; i < 100; i++) {
      final int index = i;
      executorService.execute(()->{
        System.out.println("当前线程："+Thread.currentThread()+"----------"+index);
      });
    }
  }
}
