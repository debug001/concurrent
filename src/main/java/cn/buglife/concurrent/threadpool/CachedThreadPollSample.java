package cn.buglife.concurrent.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 可缓存线程池
 * 工作线程的数量几乎没有限制，最大线程数不能超过INT最大值，即可灵活的往线程池中添加线程
 * 若长时间未往线程池中提交任务，即现成空闲的时长大道默认1分钟，则该现成将自动终止。终止后若有新的任务提交到线程池会自动创建新的线程
 * 由于对线程数量限制比较宽松，则需要我们在使用该类线程池的过程中需要谨慎控制任务的数量，以免由于启用过多的线程运行导致系统瘫痪
 *
 * Created by zhangjun on 2017/4/8.
 */
public class CachedThreadPollSample {

  public static void main(String[] args) {
    ExecutorService executorService = Executors.newCachedThreadPool();

    //给以上创建的线程池创建100个任务进去,当0,99打印完毕之后，我们会发现控制台还未自动关闭，需要等待一段时间才会关闭，正是因为”可缓冲线程池“的
    //特性，当线程空闲之后会等待默认的一分钟时段若还未有任务进入，将自动结束
    for (int i = 0; i < 100; i++) {
      final int index = i;
      executorService.execute(()-> System.out.println("当前线程:"+Thread.currentThread()+"------"+index));
      System.out.println("线程池大小："+((ThreadPoolExecutor)executorService).getPoolSize());
    }

  }
}
