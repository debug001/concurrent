package cn.buglife.concurrent.scheduled;

import java.time.LocalTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author zhangjun
 */
public class ScheduledServiceSample {

  public static void main(String[] args) {

    ScheduledExecutorService scheduledService = Executors.newScheduledThreadPool(2);

    scheduledService.scheduleAtFixedRate(() -> System.out.println(LocalTime.now()), 1, 1,
        TimeUnit.SECONDS);
    scheduledService.scheduleAtFixedRate(() -> System.out.println(LocalTime.now()), 1, 1,
        TimeUnit.SECONDS);
    scheduledService.scheduleAtFixedRate(() -> System.out.println(LocalTime.now()), 1, 1,
        TimeUnit.SECONDS);
    scheduledService.scheduleAtFixedRate(() -> System.out.println(LocalTime.now()), 1, 1,
        TimeUnit.SECONDS);
    scheduledService.scheduleAtFixedRate(() -> System.out.println(LocalTime.now()), 1, 1,
        TimeUnit.SECONDS);
  }
}
