package cn.buglife.concurrent.queue.syncronous;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;

/**
 * 
 * SynchronousQueue 是一个特殊的队列，它的内部同时只能够容纳单个元素。
 * 
 * 如果该队列已有一元素的话，试图向队列中插入一个新元素的线程将会阻塞，直到另一个线程将该元素从队列中抽走。
 * 同样，如果该队列为空，试图向队列中抽取一个元素的线程将会阻塞，直到另一个线程向队列中插入了一条新的元素。
 * 
 * @author zhangjun
 */
public class SynchronousQueueSample {

  public static void main(String[] args) {
    BlockingQueue<Integer> queue = new SynchronousQueue<>();

    Runnable producer = () -> {
      try {
        queue.put(100);
        System.out.println("等待插入第二个元素");
        queue.put(200);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    Runnable consumer = () -> {
      try {
        Thread.sleep(5000);
        System.out.println(queue.take());
        System.out.println(queue.take());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };
    new Thread(producer).start();
    new Thread(consumer).start();
  }
}
