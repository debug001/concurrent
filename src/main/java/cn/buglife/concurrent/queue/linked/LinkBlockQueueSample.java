package cn.buglife.concurrent.queue.linked;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * LinkedBlockingQueue
 * 内部以一个链式结构(链接节点)对其元素进行存储。如果需要的话，这一链式结构可以选择一个上限。如果没有定义上限，将使用 Integer.MAX_VALUE
 * 作为上限。
 *
 * <p>
 * LinkedBlockingQueue
 * 内部以FIFO(先进先出)的顺序对元素进行存储。队列中的头元素在所有元素之中是放入时间最久的那个，而尾元素则是最短的那个。
 * </p>
 * 
 * @author zhangjun
 */
public class LinkBlockQueueSample {

  public static void main(String[] args) {
    BlockingQueue<Integer> queue = new LinkedBlockingDeque<>(128);

    Runnable producer = ()->{
      try {
        queue.put(1);
        queue.put(2);
        queue.put(3);
        queue.put(4);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    Runnable consumer =()->{
      try {
        System.out.println(queue.take());
        System.out.println(queue.take());
        System.out.println(queue.take());
        System.out.println(queue.take());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    new Thread(producer).start();
    new Thread(consumer).start();
  }
}
