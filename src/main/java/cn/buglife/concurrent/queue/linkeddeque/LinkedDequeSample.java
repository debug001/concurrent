package cn.buglife.concurrent.queue.linkeddeque;

import java.util.concurrent.LinkedBlockingDeque;

/**
 * LinkedBlockingDeque 是一个双端队列，在它为空的时候，一个试图从中抽取数据的线程将会阻塞，无论该线程是试图从哪一端抽取数据。
 * 
 * @author zhangjun
 */
public class LinkedDequeSample {
  public static void main(String[] args) {
    LinkedBlockingDeque<String> deque = new LinkedBlockingDeque<>();

    Runnable consumer = ()->{
      try {
        System.out.println(deque.take());
        System.out.println(deque.take());
        System.out.println(deque.take());
        System.out.println(deque.take());
        System.out.println(deque.take());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    Runnable producer = ()->{
      try {
        deque.put("hello");
        deque.put("world");
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    new Thread(consumer).start();
    new Thread(producer).start();
  }
}
