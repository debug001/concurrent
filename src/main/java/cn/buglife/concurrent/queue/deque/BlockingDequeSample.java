package cn.buglife.concurrent.queue.deque;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * BlockingDeque 类是一个双端队列，在不能够插入元素时，它将阻塞住试图插入元素的线程；在不能够抽取元素时，它将阻塞住试图抽取的线程。
 * 
 * <p>
 * deque(双端队列) 是 "Double Ended Queue" 的缩写。因此，双端队列是一个你可以从任意一端插入或者抽取元素的队列。
 * </p>
 * 
 * <p>
 * 在线程既是一个队列的生产者又是这个队列的消费者的时候可以使用到 {@link java.util.concurrent.BlockingDeque}
 * 如果生产者线程需要在队列的两端都可以插入数据，消费者线程需要在队列的两端都可以移除数据，这个时候也可以使用 BlockingDeque。
 * </p>
 * 
 * @author zhangjun
 */
public class BlockingDequeSample {

  public static void main(String[] args) {

    BlockingDeque<String> deque = new LinkedBlockingDeque<String>();

    deque.addFirst("1");
    deque.addLast("2");
    deque.add("3");

    try {
      System.out.println(deque.takeLast());
      System.out.println(deque.takeFirst());
      System.out.println(deque.takeFirst());
      System.out.println(deque.takeFirst());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
