package cn.buglife.concurrent.queue.priority;

import java.time.LocalDateTime;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * PriorityBlockingQueue 是一个无界的并发队列。
 * 
 * <p>
 * 它使用了和类 java.util.PriorityQueue 一样的排序规则。
 * </p>
 * 
 * <p>
 * 你无法向这个队列中插入 null 值。 所有插入到 PriorityBlockingQueue 的元素必须实现 java.lang.Comparable
 * 接口。
 * </p>
 * 
 * <p>
 * 因此该队列中元素的排序就取决于你自己的 Comparable 实现。 注意 PriorityBlockingQueue
 * 对于具有相等优先级(compare() == 0)的元素并不强制任何特定行为。
 * </p>
 * 
 * @author zhangjun
 */
public class PriorityBlockQueueSample {

  public static void main(String[] args) {
    BlockingQueue<Element> queue = new PriorityBlockingQueue<>();

    Runnable producer = () -> {
      try {
        Element element = new Element("a", LocalDateTime.of(2996, 8, 11, 0, 0, 0));
        queue.put(element);
        element = new Element("b", LocalDateTime.of(1996, 8, 11, 0, 0, 0));
        queue.put(element);
        element = new Element("c", LocalDateTime.of(1997, 8, 11, 0, 0, 0));
        queue.put(element);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    Runnable consumer = () -> {
      try {
        System.out.println(queue.take());
        System.out.println(queue.take());
        System.out.println(queue.take());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    new Thread(producer).start();
    new Thread(consumer).start();
  }
}
