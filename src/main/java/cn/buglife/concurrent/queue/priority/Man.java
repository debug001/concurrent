package cn.buglife.concurrent.queue.priority;

import java.time.LocalDateTime;

/**
 * @author zhangjun
 */
public class Man {

  private String name;

  private LocalDateTime birthDay;

  public Man(String name, LocalDateTime birthDay) {
    this.name = name;
    this.birthDay = birthDay;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDateTime getBirthDay() {
    return birthDay;
  }

  public void setBirthDay(LocalDateTime birthDay) {
    this.birthDay = birthDay;
  }

  @Override
  public String toString() {
    return "name:" + name + ",birthday:" + birthDay;
  }
}
