package cn.buglife.concurrent.queue.priority;

import java.time.LocalDateTime;

/**
 * @author zhangjun
 */
public class Element extends Man implements Comparable<Man> {
  public Element(String name, LocalDateTime birthDay) {
    super(name, birthDay);
  }

  @Override
  public int compareTo(Man o) {
    return o.getBirthDay().compareTo(LocalDateTime.now());
  }
}
