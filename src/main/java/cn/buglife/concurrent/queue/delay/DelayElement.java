package cn.buglife.concurrent.queue.delay;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author zhangjun
 */
public class DelayElement implements Delayed {

  private Integer value;

  public DelayElement(Integer value) {
    this.value = value;
  }

  @Override
  public long getDelay(TimeUnit unit) {
    // DelayQueue 将会在每个元素的 getDelay() 方法返回的值的时间段之后才释放掉该元素。如果返回的是 0
    // 或者负值，延迟将被认为过期，该元素将会在 DelayQueue 的下一次 take 被调用的时候被释放掉。
    return unit.toMillis(3000);
  }

  @Override
  public int compareTo(Delayed o) {
    return 0;
  }

  public Integer getValue() {
    return value;
  }

  public void setValue(Integer value) {
    this.value = value;
  }
}
