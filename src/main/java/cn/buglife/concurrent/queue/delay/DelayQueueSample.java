package cn.buglife.concurrent.queue.delay;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;

/**
 * DelayQueue 对元素进行持有直到一个特定的延迟到期。
 * </p>
 * 注入其中的元素必须实现 java.util.concurrent.Delayed 接口
 *
 * @author zhangjun
 */
public class DelayQueueSample {

  public static void main(String[] args) {
    BlockingQueue<DelayElement> queue = new DelayQueue<>();

    Runnable producer = () -> {
      try {
        queue.put(new DelayElement(100));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    Runnable consumer = () -> {
      try {
        System.out.println(queue.take().getValue());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    new Thread(producer).start();
    new Thread(consumer).start();
  }
}
