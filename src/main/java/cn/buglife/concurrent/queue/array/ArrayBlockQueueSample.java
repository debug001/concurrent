package cn.buglife.concurrent.queue.array;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author zhangjun
 */
public class ArrayBlockQueueSample {
  public static void main(String[] args) {
    BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(512);

    Runnable producer = () -> {
      try {
        queue.put(100);
        Thread.sleep(1000);
        queue.put(200);
        Thread.sleep(1000);
        queue.put(300);
        Thread.sleep(1000);
        queue.put(400);
        Thread.sleep(1000);
        queue.put(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };

    Runnable consumer = () -> {
      try {
        System.out.println(queue.take());
        System.out.println(queue.take());
        System.out.println(queue.take());
        System.out.println(queue.take());
        System.out.println(queue.take());
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };
    new Thread(producer).start();
    new Thread(consumer).start();

    try {
      Thread.sleep(10000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
