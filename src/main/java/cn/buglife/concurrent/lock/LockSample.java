package cn.buglife.concurrent.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zhangjun
 */
public class LockSample {

  public static void main(String[] args) {
    Lock lock = new ReentrantLock();

    new Thread(() -> {
      System.out.println(Thread.currentThread().getName() + "准备上锁1");
      lock.lock();

      try {
        Thread.sleep(8000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      lock.unlock();
    }).start();

    new Thread(() -> {
      System.out.println(Thread.currentThread().getName() + "准备上锁2");
      lock.lock();
      System.out.println ("上锁成功");
      try {
        Thread.sleep(10000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }).start();
  }
}
